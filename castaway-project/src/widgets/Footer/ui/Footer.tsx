import React from 'react';
import styles from './Footer.module.scss';
import { AppIcons } from '../../AppIcons';
import { CustomNav } from '../../../shared/ui/CustomNav';
import { NavDirection } from '../../../shared/ui/CustomNav/ui/CustomNav';
import { Logo } from '../../../shared/ui/Logo';
import { LogoSizes } from '../../../shared/ui/Logo/ui/Logo';

const restInfo = [
  'Style Guide',
  'Instruction',
  'Changelog',
  'Credit',
  'Powered by Webflow',
  'Licenses',
];

export const Footer = React.memo(() => {
  return (
    <footer className={styles.footer}>
      <Logo size={LogoSizes.L_SIZE} />
      <div className={styles.info}>
        <div>
          <CustomNav directionType={NavDirection.VERTICAL} />
        </div>
        <div>
          {/*нету информации насчет данного списка, поэтому оставляю так*/}
          <ul className={styles.list}>
            {restInfo.map((info) => (
              <li key={info}>{info}</li>
            ))}
          </ul>
        </div>
      </div>
      <div>
        <AppIcons />
      </div>
    </footer>
  );
});
