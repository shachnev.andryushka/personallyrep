import { Meta, StoryObj } from '@storybook/react';
import { Episode } from './Episode';

const meta = {
  title: 'widgets/Episode',
  component: Episode,
  parameters: {},
} satisfies Meta<typeof Episode>;

export default meta;
type Story = StoryObj<typeof meta>;

export const BlackThemeEpisode: Story = {
  args: {
    episode: {
      title: 'Episode Title',
      type: 'Gear',
      series: 13,
      info: 'Episode information',
      picture:
        'https://s3-alpha-sig.figma.com/img/0993/e56f/529b6efc25efc3e321d6655835061406?Expires=1714348800&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=IZIE0N2HsG0qAqgNeCAhvTXoH47PAFuZwLQiBVMISS~TG65Fb4Rm~oV5U~wGB9iorni3K7AOkq7IY4l-MxbSqmuxytyFxN7iFIaZRcl3RCSwVGyc-w-tj84y-tq1vkh2kWMP4oH9wG3A1lBDe8niE~95CW7pBtHgx-xX2L7j5Q2OR1sNrafmC9nrFtJ2BynquOqCIfmgZVrGxQsO3797kBeG5aPpkIKfELq0NA1fARjOLcoTvEPSOVSBW-CylgZaDhtJZxcFGRmvYco6fZAj8pUS4QIKzu1iwmRMDtMt12hSqFK-T3vve1YfJ4skRz4FBdWxs9ATjQW~oVHtQrhF8g__',
    },
  },
};
