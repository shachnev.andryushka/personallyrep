import React from 'react';
import styles from './Episode.module.scss';
import { CustomButton } from '../../../shared/ui/CustomButton';
import {
  ButtonSizes,
  ButtonTheme,
} from '../../../shared/ui/CustomButton/ui/CustomButton';
import { IEpisodesData } from '../../../shared/config/episodes/episodesData';

interface IEpisodeProps {
  episode: IEpisodesData;
}

export const Episode: React.FC<IEpisodeProps> = React.memo(({ episode }) => {
  const { title, type, series, info, picture } = episode;

  return (
    <div className={styles.episode}>
      <div className={styles.episodeContainer}>
        <img src={picture} alt={picture} />
        <div className={styles.episodeInfo}>
          <div className={styles.episodeType}>
            <span>{type}</span>
          </div>
          <div className={styles.episodeNumber}>
            <span>{`Episode ${series}`}</span>
          </div>
          <div>
            <h3>{title}</h3>
          </div>
          <p>{info}</p>
          <CustomButton
            buttonTheme={ButtonTheme.PRIMARY}
            size={ButtonSizes.XL_SIZE}
          >
            View Episode Details
          </CustomButton>
        </div>
      </div>
    </div>
  );
});
