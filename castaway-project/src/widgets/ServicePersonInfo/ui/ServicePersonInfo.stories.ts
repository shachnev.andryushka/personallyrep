import { Meta, StoryObj } from '@storybook/react';
import { ServicePersonInfo } from './ServicePersonInfo';

const meta = {
  title: 'widgets/ServicePersonInfo',
  component: ServicePersonInfo,
  parameters: {},
} satisfies Meta<typeof ServicePersonInfo>;

export default meta;
type Story = StoryObj<typeof meta>;

export const BlackThemeHeader: Story = {
  args: {},
};
