import React from 'react';
import styles from './ServicePersonInfo.module.scss';
import { ReactComponent as PersonInfoImg } from '../../../shared/assets/ServiceInfoImgsvg.svg';
import { RoundButton } from '../../../shared/ui/RoundButton';

export const ServicePersonInfo = React.memo(() => {
  return (
    <div className={styles.personInfoContainer}>
      <div className={styles.info}>
        <div>
          <RoundButton />
          <div className={styles.labelBlock}>
            <span className={styles.label}>Meet your host</span>
          </div>
        </div>
        <h4>Jacob Paulaner</h4>
        <p>
          Jacob has a background in audio engineering, and has been podcasting
          since the early days.
        </p>
        <p>
          He’s here to help you level up your game by sharing everything he’s
          learned along the way.
        </p>
      </div>
      <div>
        <PersonInfoImg className={styles.personInfoImg} />
      </div>
    </div>
  );
});
