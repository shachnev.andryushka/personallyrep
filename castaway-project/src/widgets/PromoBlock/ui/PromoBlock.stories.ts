import { Meta, StoryObj } from '@storybook/react';
import { PromoBlock } from './PromoBlock';

const meta = {
  title: 'widgets/PromoBlock',
  component: PromoBlock,
  parameters: {},
} satisfies Meta<typeof PromoBlock>;

export default meta;
type Story = StoryObj<typeof meta>;

export const BlackThemeHeader: Story = {
  args: {},
};
