import React from 'react';
import styles from './PromoBlock.module.scss';
import { ReactComponent as Lines } from '../../../shared/assets/LinesPromoImg.svg';
import { ReactComponent as PromoImg } from '../../../shared/assets/PromoImg.svg';
import { AppIcons } from '../../AppIcons';

export const PromoBlock = React.memo(() => {
  return (
    <div className={styles.promoBlock}>
      <div className={styles.promiImg}>
        <Lines className={styles.lines} />
        <PromoImg />
      </div>
      <div className={styles.title}>
        <p>
          Take your podcast to the next <b>level</b>
        </p>
        <div>
          <span>Listen On</span>
          <AppIcons />
        </div>
      </div>
    </div>
  );
});
