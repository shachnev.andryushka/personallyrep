import React, { useCallback, useState } from 'react';
import styles from './SubscriptionForm.module.scss';
import { CustomInput } from '../../../shared/ui/CustomInput';
import { CustomButton } from '../../../shared/ui/CustomButton';
import {
  ButtonSizes,
  ButtonTheme,
} from '../../../shared/ui/CustomButton/ui/CustomButton';

export const SubscriptionForm = React.memo(() => {
  const [formData, setFormData] = useState({ name: '', email: '' });

  const onChangeName = useCallback(
    (value: string) => {
      setFormData({ ...formData, name: value });
    },
    [formData]
  );

  const onChangeEmail = useCallback(
    (value: string) => {
      setFormData({ ...formData, email: value });
    },
    [formData]
  );

  return (
    <div className={styles.subscriptionForm}>
      <div className={styles.aboutSubscription}>
        <div>
          <span>Email Newsletter</span>
        </div>
        <h4>Subscribe for updates</h4>
      </div>
      <form>
        <CustomInput
          value={formData.name}
          onChange={onChangeName}
          className={styles.input}
          placeholder="Name"
        />
        <CustomInput
          value={formData.email}
          onChange={onChangeEmail}
          className={styles.input}
          type="email"
          placeholder="Email"
        />
        <CustomButton
          buttonTheme={ButtonTheme.PRIMARY}
          size={ButtonSizes.L_SIZE}
        >
          Submit
        </CustomButton>
      </form>
    </div>
  );
});
