import { Meta, StoryObj } from '@storybook/react';
import { SubscriptionForm } from './SubscriptionForm';

const meta = {
  title: 'widgets/SubscriptionForm',
  component: SubscriptionForm,
  parameters: {
    layout: 'centered',
  },
} satisfies Meta<typeof SubscriptionForm>;

export default meta;
type Story = StoryObj<typeof meta>;

export const BlackThemeLatestEpisodesList: Story = {
  args: {},
};
