import { Meta, StoryObj } from '@storybook/react';
import { AppIcons } from './AppIcons';

const meta = {
  title: 'widgets/AppIcons',
  component: AppIcons,
  parameters: {},
} satisfies Meta<typeof AppIcons>;

export default meta;
type Story = StoryObj<typeof meta>;

export const AppItems: Story = {
  args: {},
};
