import React from 'react';
import styles from './AppIcons.module.scss';
import { ReactComponent as Spotify } from '../../../shared/assets/messangersImg/Spotify.svg';
import { ReactComponent as ImgApp1 } from '../../../shared/assets/messangersImg/unknown1.svg';
import { ReactComponent as ImgApp2 } from '../../../shared/assets/messangersImg/unknown2.svg';
import { ReactComponent as ImgApp3 } from '../../../shared/assets/messangersImg/unknown3.svg';
import { ReactComponent as ImgApp4 } from '../../../shared/assets/messangersImg/unknown4.svg';

export const AppIcons = React.memo(() => {
  return (
    <div className={styles.messengers}>
      <Spotify className={styles.linkImg} />
      <ImgApp1 className={styles.linkImg} />
      <ImgApp2 className={styles.linkImg} />
      <ImgApp3 className={styles.linkImg} />
      <ImgApp4 className={styles.linkImg} />
    </div>
  );
});
