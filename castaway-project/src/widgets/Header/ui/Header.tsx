import React from 'react';
import styles from './Header.module.scss';
import { Logo } from '../../../shared/ui/Logo';
import { LogoSizes } from '../../../shared/ui/Logo/ui/Logo';
import { CustomNav } from '../../../shared/ui/CustomNav';
import { NavDirection } from '../../../shared/ui/CustomNav/ui/CustomNav';

export const Header = React.memo(() => {
  return (
    <header className={styles.header}>
      <div>
        <Logo size={LogoSizes.L_SIZE} />
      </div>
      <CustomNav directionType={NavDirection.NONE} />
    </header>
  );
});
