import React from 'react';
import styles from './LoginForm.module.scss';
import { CustomInput } from '../../../shared/ui/CustomInput';
import { CustomButton } from '../../../shared/ui/CustomButton';
import {
  ButtonSizes,
  ButtonTheme,
} from '../../../shared/ui/CustomButton/ui/CustomButton';

export const LoginForm = React.memo(() => {
  return (
    <form className={styles.loginForm}>
      <CustomInput className={styles.loginFormInput} placeholder="Имя" />
      <CustomInput
        className={styles.loginFormInput}
        type="password"
        placeholder="Введите пароль"
      />
      <CustomButton
        type="submit"
        buttonTheme={ButtonTheme.PRIMARY}
        size={ButtonSizes.S_SIZE}
      >
        Вход
      </CustomButton>
    </form>
  );
});
