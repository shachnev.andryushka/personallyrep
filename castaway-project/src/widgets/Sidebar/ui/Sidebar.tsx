import React, { useCallback, useContext, useState } from 'react';
import styles from './Sidebar.module.scss';
import { FaArrowsAltH } from 'react-icons/fa';
import { WiDaySunny } from 'react-icons/wi';
import { CustomButton } from '../../../shared/ui/CustomButton';
import {
  ButtonSizes,
  ButtonTheme,
} from '../../../shared/ui/CustomButton/ui/CustomButton';
import { classNames } from '../../../shared/lib/classNames/classNames';
import { Modal } from '../../../shared/ui/Modal';
import { LoginForm } from '../../LoginForm';
import {
  Theme,
  ThemeContext,
} from '../../../app/providers/ThemeProvider/lib/ThemeContext';

export const Sidebar = React.memo(() => {
  const [expanded, setExpanded] = useState(false);
  const [openModal, setOpenModal] = useState(false);

  const onClickExpandSidebar = useCallback(() => {
    setExpanded((prev) => !prev);
  }, []);

  const onToggleOpenModal = useCallback(() => {
    setOpenModal((prev) => !prev);
  }, []);

  const { theme, toggleTheme } = useContext(ThemeContext);

  const iconThemeColor =
    theme === Theme.DARK ? 'yellowIconColor' : 'bluevioletIconColor';

  return (
    <aside
      className={classNames(styles.sidebar, { [styles.isExpand]: expanded })}
    >
      <div className={styles.container}>
        {expanded && (
          <CustomButton
            buttonTheme={ButtonTheme.PRIMARY}
            onClick={onToggleOpenModal}
            size={ButtonSizes.S_SIZE}
          >
            Вход
          </CustomButton>
        )}
        <CustomButton
          className={styles.themeIconBtn}
          size={ButtonSizes.NONE}
          buttonTheme={ButtonTheme.EMPTY}
          onClick={() => toggleTheme(theme)}
        >
          <WiDaySunny
            className={classNames(styles.themeIcon, {}, [
              styles[iconThemeColor],
            ])}
          />
        </CustomButton>
      </div>
      <div className={styles.expandedContainer}>
        <CustomButton
          buttonTheme={ButtonTheme.PRIMARY}
          onClick={onClickExpandSidebar}
          className={styles.btnSize}
          size={ButtonSizes.NONE}
        >
          <FaArrowsAltH />
        </CustomButton>
      </div>
      {openModal && (
        <Modal onClick={onToggleOpenModal}>
          <LoginForm />
        </Modal>
      )}
    </aside>
  );
});
