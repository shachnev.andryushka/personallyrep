import React, { FC } from 'react';
import styles from './Review.module.scss';
import { ReactComponent as ReviewStar } from '../../../shared/assets/reviewStar.svg';
import { IReviews } from '../../../shared/config/reviews/reviews';

interface IReviewProps {
  review: IReviews;
}

export const Review: FC<IReviewProps> = React.memo(({ review }) => {
  const { evaluation, author, comment } = review;

  return (
    <div className={styles.review}>
      <div className={styles.evaluation}>
        <ReviewStar />
        {evaluation}
      </div>
      <p>{comment}</p>
      <div>
        <span>{author}</span>
      </div>
    </div>
  );
});
