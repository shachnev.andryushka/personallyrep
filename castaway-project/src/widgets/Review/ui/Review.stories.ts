import { Meta, StoryObj } from '@storybook/react';
import { Review } from './Review';

const meta = {
  title: 'widgets/Review',
  component: Review,
  parameters: {
    layout: 'centered',
  },
} satisfies Meta<typeof Review>;

export default meta;
type Story = StoryObj<typeof meta>;

export const BlackThemeReview: Story = {
  args: {
    review: {
      evaluation: 5,
      author: 'Morgoth Bauglir',
      comment: 'The Dark Lord is the best',
    },
  },
};
