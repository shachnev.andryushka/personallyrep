import React, { Suspense, useContext } from 'react';
import '../app/styles/global.scss';
import { Header } from '../widgets/Header';
import { AppRouter } from './providers/router';
import { Sidebar } from '../widgets/Sidebar';
import { ThemeContext } from './providers/ThemeProvider/lib/ThemeContext';
import { classNames } from '../shared/lib/classNames/classNames';

function App() {
  const { theme } = useContext(ThemeContext);

  return (
    <Suspense fallback={<div>Loading...</div>}>
      <div className={classNames('app', {}, [theme])}>
        <Header />
        <Sidebar />
        <AppRouter />
      </div>
    </Suspense>
  );
}

export default App;
