import { configureStore } from '@reduxjs/toolkit';
import { episodesReducer } from '../../../entities/episodes/model/slice/episodesSlice';
import { reviewsReducer } from '../../../entities/reviews/model/slice/reviewsSlice';

export const store = configureStore({
  reducer: {
    episodesReducer,
    reviewsReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
