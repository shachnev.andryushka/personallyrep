import { createContext } from 'react';

export enum Theme {
  LIGHT = 'light_theme',
  DARK = 'dark_teme',
}

export interface IThemeContext {
  theme: Theme;
  toggleTheme: (theme: Theme) => void;
}

export const ThemeContext = createContext<IThemeContext>({
  theme: Theme.DARK,
  toggleTheme: (theme) => {},
});

export const LOCALE_STORAGE_THEME_KEY = 'theme';
