import { useCallback, useState } from 'react';
import { LOCALE_STORAGE_THEME_KEY, Theme } from './ThemeContext';

interface IUseThemeResult {
  theme: Theme;
  toggleTheme: (theme: Theme) => void;
}

const currentTheme: Theme =
  (localStorage.getItem(LOCALE_STORAGE_THEME_KEY) as Theme) || Theme.DARK;

export const useTheme = (): IUseThemeResult => {
  const [theme, setTheme] = useState<Theme>(currentTheme);

  const toggleTheme = useCallback((theme: Theme) => {
    const newTheme = theme === Theme.DARK ? Theme.LIGHT : Theme.DARK;

    localStorage.setItem(LOCALE_STORAGE_THEME_KEY, newTheme);
    console.log(newTheme);
    setTheme(newTheme);
  }, []);

  return {
    theme,
    toggleTheme,
  };
};
