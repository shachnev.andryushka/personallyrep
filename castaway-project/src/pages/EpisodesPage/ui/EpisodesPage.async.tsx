import { lazy } from 'react';

export const EpisodesPageAsync = lazy(() => import('./EpisodesPage'));
