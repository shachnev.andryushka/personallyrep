import React from 'react';
import { PromoBlock } from '../../../widgets/PromoBlock';
import { ServicePersonInfo } from '../../../widgets/ServicePersonInfo';
import { SubscriptionForm } from '../../../widgets/SubscriptionForm';
import { Footer } from '../../../widgets/Footer';
import { LatestEpisodesList } from '../../../entities/episodes/ui/LatestEpisodesList';
import { ReviewList } from '../../../entities/reviews/ui/ReviewList';

const HomePage = () => {
  return (
    <div>
      <PromoBlock />
      <LatestEpisodesList />
      <ServicePersonInfo />
      <SubscriptionForm />
      <ReviewList />
      <Footer />
    </div>
  );
};

export default HomePage;
