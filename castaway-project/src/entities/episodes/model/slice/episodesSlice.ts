import { PayloadAction, SerializedError, createSlice } from '@reduxjs/toolkit';
import { getAllEpisodes } from '../services/getAllEpisodes';

export interface IEpisodesData {
  type: string;
  series: number;
  title: string;
  info: string;
  picture: string;
}

interface IEpisodesState {
  episodes: IEpisodesData[];
  isLoading: boolean;
  error: SerializedError | null;
}

const initialState: IEpisodesState = {
  episodes: [],
  isLoading: false,
  error: null,
};

export const episodesSlice = createSlice({
  name: 'episodes',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getAllEpisodes.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(
      getAllEpisodes.fulfilled,
      (state, action: PayloadAction<IEpisodesData[]>) => {
        state.isLoading = false;
        state.episodes = action.payload;
      }
    );
    builder.addCase(getAllEpisodes.rejected, (state, action) => {
      state.isLoading = false;
      state.error = action.error;
    });
  },
});

export const { actions: episodesActions } = episodesSlice;

export const { reducer: episodesReducer } = episodesSlice;
