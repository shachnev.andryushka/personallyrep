import { createAsyncThunk } from '@reduxjs/toolkit';
import axios, { AxiosError } from 'axios';

export const getAllEpisodes = createAsyncThunk(
  'episodes/getAllEpisodes',
  async (_, thunkAPI) => {
    try {
      const response = await axios.get('http://localhost:8000/episodes');
      return response.data;
    } catch (error: unknown) {
      return thunkAPI.rejectWithValue(error as AxiosError);
    }
  }
);
