import { Meta, StoryObj } from '@storybook/react';
import { LatestEpisodesList } from './LatestEpisodesList';

const meta = {
  title: 'widgets/LatestEpisodesList',
  component: LatestEpisodesList,
  parameters: {},
} satisfies Meta<typeof LatestEpisodesList>;

export default meta;
type Story = StoryObj<typeof meta>;

export const BlackThemeLatestEpisodesList: Story = {
  args: {},
};
