import styles from './LatestEpizodesList.module.scss';

import { useNavigate } from 'react-router-dom';
import { useCallback, useEffect } from 'react';
import React from 'react';
import { getAllEpisodes } from '../../../model/services/getAllEpisodes';
import { CustomButton } from '../../../../../shared/ui/CustomButton';
import {
  ButtonSizes,
  ButtonTheme,
} from '../../../../../shared/ui/CustomButton/ui/CustomButton';
import { Episode } from '../../../../../widgets/Episode';
import {
  useAppDispatch,
  useAppSelector,
} from '../../../../../app/providers/StoreProvider/storeHook';

export const LatestEpisodesList = React.memo(() => {
  const episodes = useAppSelector((state) => state.episodesReducer.episodes);
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const redirectToEpisodesPage = useCallback(() => {
    navigate('/episodes');
  }, [navigate]);

  useEffect(() => {
    dispatch(getAllEpisodes());
  }, [dispatch]);

  return (
    <div>
      <div className={styles.title}>
        <h2>Latest Episodes</h2>
        <CustomButton
          buttonTheme={ButtonTheme.PRIMARY}
          onClick={redirectToEpisodesPage}
          size={ButtonSizes.L_SIZE}
        >
          View all episodes
        </CustomButton>
      </div>
      <div className={styles.lastEpisodesList}>
        {episodes.map((episode) => (
          <Episode key={episode.picture} episode={episode} />
        ))}
      </div>
    </div>
  );
});
