import { PayloadAction, SerializedError, createSlice } from '@reduxjs/toolkit';
import { getReviews } from '../services/getReviews';

export interface IReviews {
  evaluation: number;
  comment: string;
  author: string;
}

interface IReviewsState {
  isLoading: boolean;
  error: SerializedError | null;
  reviews: IReviews[];
}

const initialState: IReviewsState = {
  isLoading: false,
  error: null,
  reviews: [],
};

export const reviewsSlice = createSlice({
  name: 'reviews',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getReviews.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(
      getReviews.fulfilled,
      (state, action: PayloadAction<IReviews[]>) => {
        state.isLoading = false;
        state.reviews = action.payload;
      }
    );
    builder.addCase(getReviews.rejected, (state, action) => {
      state.isLoading = false;
      state.error = action.error;
    });
  },
});

export const { actions: reviewsActions } = reviewsSlice;

export const { reducer: reviewsReducer } = reviewsSlice;
