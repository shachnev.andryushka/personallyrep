import { createAsyncThunk } from '@reduxjs/toolkit';
import axios, { AxiosError } from 'axios';

export const getReviews = createAsyncThunk(
  'reviews/getReviews',
  async (_, thunkAPI) => {
    try {
      const response = await axios.get('http://localhost:8000/reviews');
      return response.data;
    } catch (error: unknown) {
      return thunkAPI.rejectWithValue(error as AxiosError);
    }
  }
);
