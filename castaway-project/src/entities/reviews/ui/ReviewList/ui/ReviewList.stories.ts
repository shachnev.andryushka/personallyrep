import { Meta, StoryObj } from '@storybook/react';
import { ReviewList } from './ReviewList';

const meta = {
  title: 'widgets/ReviewList',
  component: ReviewList,
  parameters: {
    layout: 'centered',
  },
} satisfies Meta<typeof ReviewList>;

export default meta;
type Story = StoryObj<typeof meta>;

export const BlackThemeLatestEpisodesList: Story = {
  args: {},
};
