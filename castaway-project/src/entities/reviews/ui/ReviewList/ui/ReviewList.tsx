import React, { useEffect } from 'react';
import styles from './ReviewList.module.scss';
import {
  useAppDispatch,
  useAppSelector,
} from '../../../../../app/providers/StoreProvider/storeHook';
import { getReviews } from '../../../model/services/getReviews';
import { Review } from '../../../../../widgets/Review';

export const ReviewList = React.memo(() => {
  const reviews = useAppSelector((state) => state.reviewsReducer.reviews);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getReviews());
  }, [dispatch]);

  return (
    <div className={styles.reviewList}>
      {reviews.map((review) => (
        <Review key={review.comment} review={review} />
      ))}
    </div>
  );
});
