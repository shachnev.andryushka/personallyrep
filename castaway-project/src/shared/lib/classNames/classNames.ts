export type ModsType = Record<string, boolean | string | undefined>;

export const classNames = (
  className: string,
  mods: ModsType = {},
  additionally: string[] = []
) => {
  return [
    className,
    ...additionally.filter(Boolean),
    ...Object.entries(mods)
      .filter(([_, value]) => Boolean(value))
      .map(([className, _]) => className),
  ].join(' ');
};
