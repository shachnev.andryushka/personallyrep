import { classNames } from './classNames';

describe('classNames', () => {
  test('Positive arguments', () => {
    const expected = 'className1 firstAdditionally mod';

    expect(classNames('className1', { mod: true }, ['firstAdditionally'])).toBe(
      expected
    );
  });

  test('Negative arguments', () => {
    const expected = 'className1 selector';
    expect(classNames('className1', { mod: false }, ['selector'])).toBe(
      expected
    );
  });

  test('without mods and additionally', () => {
    const expected = 'className1';

    expect(classNames('className1')).toBe(expected);
  });
});
