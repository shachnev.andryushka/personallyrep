export interface IEpisodesData {
  type: string;
  series: number;
  title: string;
  info: string;
  picture: string;
}

export const episodes: IEpisodesData[] = [
  {
    type: 'Gear',
    series: 3,
    title: 'Should you get outboard audio gear?',
    info: 'Is hardware really worth it when it comes to podcasting? The answer is...it depends. Here’s our reasons on why you might want to consider picking something up.',
    picture:
      'https://s3-alpha-sig.figma.com/img/0993/e56f/529b6efc25efc3e321d6655835061406?Expires=1714348800&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=IZIE0N2HsG0qAqgNeCAhvTXoH47PAFuZwLQiBVMISS~TG65Fb4Rm~oV5U~wGB9iorni3K7AOkq7IY4l-MxbSqmuxytyFxN7iFIaZRcl3RCSwVGyc-w-tj84y-tq1vkh2kWMP4oH9wG3A1lBDe8niE~95CW7pBtHgx-xX2L7j5Q2OR1sNrafmC9nrFtJ2BynquOqCIfmgZVrGxQsO3797kBeG5aPpkIKfELq0NA1fARjOLcoTvEPSOVSBW-CylgZaDhtJZxcFGRmvYco6fZAj8pUS4QIKzu1iwmRMDtMt12hSqFK-T3vve1YfJ4skRz4FBdWxs9ATjQW~oVHtQrhF8g__',
  },
  {
    type: 'Tips & Tricks',
    series: 2,
    title: 'Mic tricks to take you to the next level',
    info: 'Stop rolling with those default settings on your mic. These small tweaks will take you from sounding good to great.',
    picture:
      'https://s3-alpha-sig.figma.com/img/2dc7/4b40/297660480ff9140760d8f8d49c3b802b?Expires=1714348800&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=dGDELjKEHiwrcA4LVxh6Ei1MduYa2ADlNQ6MWWQeeI6p3VLYhOtVXO5PZ9gOPYYXCLPDT5AzvZWILEXnCLzyCpsQqgft3Rrd0rM~qbK1b8XaQyvSgHsXJ5MZiQkCkNpVH4rSKVT7AkSl44RMELK7pUb3xQGDVVhqlpBKXgHIYtrRnW8Tp-HBc2inNK6d1uRwaWhJ-bxisuYUkVgMEE8Wfx~ouAHVODq6J~phJKCXTR~7IXRz8dNGMKIIpNRd3P~HXHEU6lHh6MICjJGNFWW6TwldQiXMsxOECIyRahw5sHzXPb-LTQ66ntfO5OXqrVbY7Gia5makrJi1ukQMYMM3vw__',
  },
  {
    type: 'Gear',
    series: 1,
    title: 'The best microphone under $200',
    info: 'With so many microphones on the market, how are you supposed to know what’s the best? Take a look at our top picks.',
    picture:
      'https://s3-alpha-sig.figma.com/img/e1c7/f584/97be37456f84917e644d4c5caa3c2850?Expires=1714348800&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4&Signature=Ap2cwWQ4TAhAT~fE5Dl7WHy-qEHazqNxt42kX~vm86seBX2eTw46~w~kw1FB4-nYRtXdbavhKcQXYxeySB4kcguEs47rez5HeQaMOXzpY9jcrVClB5jxzJ0IzY2dOQjTnnCZtpYVcRrhShbBUbbuaXkDneHaFYGgd8pGdoUV9ibSwXsB14rdbLO10Iq3YFXoylDHuWNmqdizKJCab8b97QytB~omQQI9zz0UH427plfvk6KrU6QF~WhmhHCFUChMQ6CvkJaEw7ELPLq-cb-fC-ye6Kcmw8ny8DO~tWUJVTRtAk6osX0Gqj0zYFiHTYzj3F3ec8ZqZC6R3ZQxp5BNyA__',
  },
];
