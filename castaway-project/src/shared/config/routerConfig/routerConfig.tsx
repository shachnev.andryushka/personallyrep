import { RouteProps } from 'react-router-dom';
import { AboutPage } from '../../../pages/AboutPage';
import { ContactPage } from '../../../pages/ContactPage';
import { EpisodesPage } from '../../../pages/EpisodesPage';
import { HomePage } from '../../../pages/HomePage';
import NotFoundPage from '../../../pages/NotFoundPage/ui/NotFoundPage';

export enum AppRoutes {
  HOME = 'home',
  ABOUT = 'about',
  CONTACT = 'contact',
  EPISODES = 'episodes',

  NOT_FOUND = 'notFound',
}

export const routePath: Record<AppRoutes, string> = {
  [AppRoutes.HOME]: '/',
  [AppRoutes.ABOUT]: '/about',
  [AppRoutes.CONTACT]: '/contact',
  [AppRoutes.EPISODES]: '/episodes',
  [AppRoutes.NOT_FOUND]: '*',
};

export const routeConfig: Record<AppRoutes, RouteProps> = {
  [AppRoutes.HOME]: {
    path: routePath.home,
    element: <HomePage />,
  },
  [AppRoutes.ABOUT]: {
    path: routePath.about,
    element: <AboutPage />,
  },

  [AppRoutes.CONTACT]: {
    path: routePath.contact,
    element: <ContactPage />,
  },
  [AppRoutes.EPISODES]: {
    path: routePath.episodes,
    element: <EpisodesPage />,
  },
  [AppRoutes.NOT_FOUND]: {
    path: routePath.notFound,
    element: <NotFoundPage />,
  },
};
