export interface IReviews {
  evaluation: number;
  comment: string;
  author: string;
}

export const reviews: IReviews[] = [
  {
    evaluation: 5,
    comment: 'I can’t recommend this podcast enough',
    author: 'Betty Lacey',
  },
  {
    evaluation: 5,
    comment: 'Jacob is the best in the business',
    author: 'Adam Driver',
  },
  {
    evaluation: 5,
    comment: 'A wealth of audio knowledge',
    author: 'Marcus Brown',
  },
  {
    evaluation: 5,
    comment: 'Every episode is a gem!',
    author: 'Jessica Knowl',
  },
  {
    evaluation: 5,
    comment: 'Whoa whoa, let me take some notes!',
    author: 'Scott Adams',
  },
  {
    evaluation: 5,
    comment: 'I’ve upped my game considerably since I started listening',
    author: 'Steven Blast',
  },
];
