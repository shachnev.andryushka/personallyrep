import { Meta, StoryObj } from '@storybook/react';
import { RoundButton } from './RoundButton';

const meta = {
  title: 'shared/RoundButton',
  component: RoundButton,
  parameters: {
    layout: 'centered',
  },
} satisfies Meta<typeof RoundButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const ButtonSsize: Story = {
  args: {},
};
