import React, { ButtonHTMLAttributes } from 'react';
import styles from './RoundButton.module.scss';
import { MdOutlineArrowRightAlt } from 'react-icons/md';

interface IRoundButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {}

export const RoundButton: React.FC<IRoundButtonProps> = React.memo((props) => {
  return (
    <button {...props} className={styles.roundBtn}>
      <MdOutlineArrowRightAlt className={styles.arrow} />
    </button>
  );
});
