import { Meta, StoryObj } from '@storybook/react';
import { CustomNav, NavDirection } from './CustomNav';

const meta = {
  title: 'shared/CustomNav',
  component: CustomNav,
  parameters: {
    layout: 'centered',
  },
} satisfies Meta<typeof CustomNav>;

export default meta;
type Story = StoryObj<typeof meta>;

export const NoneDirection: Story = {
  args: {
    directionType: NavDirection.NONE,
  },
};

export const VerticalDirection: Story = {
  args: {
    directionType: NavDirection.VERTICAL,
  },
};
