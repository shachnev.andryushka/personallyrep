import React from 'react';
import styles from './CustomNav.module.scss';
import { CustomLink } from '../../CustomLink';

export enum NavDirection {
  VERTICAL = 'vertical',
  NONE = 'none',
}

interface IPage {
  title: string;
  to: string;
}

export const PAGES: IPage[] = [
  { title: 'Home', to: '/' },
  { title: 'Episodes', to: '/episodes' },
  { title: 'About', to: '/about' },
  { title: 'Contact', to: '/contact' },
];

interface ICustomNavProps {
  directionType: NavDirection;
}

export const CustomNav: React.FC<ICustomNavProps> = React.memo(
  ({ directionType }) => {
    return (
      <nav className={styles[directionType]}>
        {PAGES.map((page) => (
          <CustomLink key={page.to} to={page.to}>
            {page.title}
          </CustomLink>
        ))}
      </nav>
    );
  }
);
