import { Meta, StoryObj } from '@storybook/react';
import { CustomInput } from './CustomInput';

const meta = {
  title: 'shared/CustomInput',
  component: CustomInput,
  parameters: {
    layout: 'centered',
    backgrounds: { default: 'dark' },
  },
} satisfies Meta<typeof CustomInput>;

export default meta;
type Story = StoryObj<typeof meta>;

export const BlackThemeInput: Story = {
  args: {
    placeholder: 'Name',
    value: '',
    onChange: () => {},
  },
};
