import React, { HTMLAttributes } from 'react';
import styles from './CustomInput.module.scss';
import { classNames } from '../../../lib/classNames/classNames';

type HTMLInputProps = Omit<
  HTMLAttributes<HTMLInputElement>,
  'onChange' | 'value'
>;

interface ICustomInputProps extends HTMLInputProps {
  placeholder: string;
  type?: string;
  className?: string;
  value?: string;
  onChange?: (value: string) => void;
}

export const CustomInput: React.FC<ICustomInputProps> = React.memo((props) => {
  const {
    placeholder,
    type = 'text',
    className = '',
    value,
    onChange,
    ...restProps
  } = props;

  return (
    <input
      type={type}
      className={classNames(className, {}, [styles.input])}
      value={value}
      onChange={(event) => onChange && onChange(event.target.value)}
      placeholder={placeholder}
      {...restProps}
    />
  );
});
