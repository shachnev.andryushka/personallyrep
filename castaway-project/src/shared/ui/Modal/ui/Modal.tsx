import React, { ReactNode, useCallback, useContext, useEffect } from 'react';
import styles from './Modal.module.scss';
import { Portal } from '../../Portal';
import { classNames } from '../../../lib/classNames/classNames';
import { ThemeContext } from '../../../../app/providers/ThemeProvider/lib/ThemeContext';

interface IModalProps {
  children: ReactNode;
  onClick: () => void;
}

export const Modal: React.FC<IModalProps> = React.memo((props) => {
  const { children, onClick } = props;

  const { theme } = useContext(ThemeContext);

  const closeModal = useCallback(() => {
    onClick();
  }, [onClick]);

  const onKeyDown = useCallback(
    (e: KeyboardEvent) => {
      if (e.key === 'Escape') {
        closeModal();
      }
    },
    [closeModal]
  );

  useEffect(() => {
    window.addEventListener('keydown', onKeyDown);

    return () => {
      window.removeEventListener('keydown', onKeyDown);
    };
  }, [onKeyDown]);

  return (
    <Portal>
      <div
        onClick={closeModal}
        className={classNames(styles.overlay, {}, [theme])}
      >
        <div
          onClick={(event) => event.stopPropagation()}
          className={styles.content}
        >
          {children}
        </div>
      </div>
    </Portal>
  );
});
