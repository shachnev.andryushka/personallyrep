import { Meta, StoryObj } from '@storybook/react';
import { CustomLink } from './CustomLink';

const meta = {
  title: 'shared/CustomLink',
  component: CustomLink,
  parameters: {
    layout: 'centered',
  },
} satisfies Meta<typeof CustomLink>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Primary: Story = {
  args: {
    children: 'Link',
    to: '/',
  },
};
