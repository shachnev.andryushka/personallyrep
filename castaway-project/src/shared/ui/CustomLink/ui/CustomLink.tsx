import React, { ReactNode, memo } from 'react';
import style from './CustomLink.module.scss';
import { NavLink } from 'react-router-dom';

interface ICustomLinkProps {
  children: ReactNode;
  to: string;
}

export const CustomLink: React.FC<ICustomLinkProps> = memo(
  ({ children, to }) => {
    return (
      <NavLink className={style.link} to={to}>
        {children}
      </NavLink>
    );
  }
);
