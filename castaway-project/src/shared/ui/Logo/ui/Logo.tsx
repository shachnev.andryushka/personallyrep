import React, { useCallback } from 'react';
import styles from './Logo.module.scss';
import { ReactComponent as LogoComponent } from '../../../../shared/assets/Logo.svg';
import { classNames } from '../../../lib/classNames/classNames';
import { useLocation, useNavigate } from 'react-router-dom';

export enum LogoSizes {
  L_SIZE = 'lSize',
  S_SIZE = 'sSize',
}

interface ILogoProps {
  size: LogoSizes;
}

export const Logo: React.FC<ILogoProps> = React.memo(({ size }) => {
  const navigate = useNavigate();
  const location = useLocation();

  const redirectToHomePage = useCallback(() => {
    if (location.pathname === '/') {
      window.scrollTo({ top: 0, behavior: 'smooth' });
      return;
    }

    navigate('/');
  }, [location.pathname, navigate]);

  return (
    <LogoComponent
      onClick={redirectToHomePage}
      className={classNames(styles.logo, {}, [styles[size]])}
    />
  );
});
