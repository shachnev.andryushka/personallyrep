import { Meta, StoryObj } from '@storybook/react';
import { Logo, LogoSizes } from './Logo';

const meta = {
  title: 'shared/Logo',
  component: Logo,
  parameters: {
    layout: 'centered',
  },
} satisfies Meta<typeof Logo>;

export default meta;
type Story = StoryObj<typeof meta>;

export const LogoLsize: Story = {
  args: {
    size: LogoSizes.L_SIZE,
  },
};

export const LogoSsize: Story = {
  args: {
    size: LogoSizes.S_SIZE,
  },
};
