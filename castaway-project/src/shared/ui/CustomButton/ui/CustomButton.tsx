import React, { ButtonHTMLAttributes, ReactNode } from 'react';
import styles from './CustomButton.module.scss';
import { classNames } from '../../../lib/classNames/classNames';

export enum ButtonSizes {
  S_SIZE = 'sSize',
  L_SIZE = 'lSize',
  XL_SIZE = 'xlSize',
  NONE = 'none',
}

export enum ButtonTheme {
  PRIMARY = 'primary',
  EMPTY = 'empty',
}

interface ICustomButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  children?: ReactNode;
  size: ButtonSizes;
  className?: string;
  buttonTheme: ButtonTheme;
}

export const CustomButton: React.FC<ICustomButtonProps> = React.memo(
  (props) => {
    const { children, size, onClick, className = '', buttonTheme } = props;

    return (
      <button
        onClick={onClick}
        className={classNames(className, {}, [
          styles[size],
          styles[buttonTheme],
          styles.common,
        ])}
      >
        {children}
      </button>
    );
  }
);
