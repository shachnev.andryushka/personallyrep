import { Meta, StoryObj } from '@storybook/react';
import { ButtonSizes, ButtonTheme, CustomButton } from './CustomButton';

const meta = {
  title: 'shared/CustomButton',
  component: CustomButton,
  parameters: {
    layout: 'centered',
  },
} satisfies Meta<typeof CustomButton>;

export default meta;
type Story = StoryObj<typeof meta>;

export const ButtonSsize: Story = {
  args: {
    children: 'CustumBtn',
    size: ButtonSizes.S_SIZE,
    buttonTheme: ButtonTheme.PRIMARY,
  },
};

export const ButtonLsize: Story = {
  args: {
    children: 'CustumBtn',
    size: ButtonSizes.L_SIZE,
    buttonTheme: ButtonTheme.PRIMARY,
  },
};

export const ButtonXlSize: Story = {
  args: {
    children: 'CustumBtn',
    size: ButtonSizes.XL_SIZE,
    buttonTheme: ButtonTheme.PRIMARY,
  },
};
